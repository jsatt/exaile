Welcome to Exaile's documentation!
==================================

Core:

.. toctree::
    :glob:

    xl/*

GUI:

.. toctree::
    :glob:

    xlgui/*

Indices and tables
==================

* :ref:`modindex`
* :ref:`search`

